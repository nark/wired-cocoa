//
//  WWAppDelegate.m
//  WebWired
//
//  Created by Rafaël Warnault on 16/02/13.
//  Copyright (c) 2013 read-write.fr. All rights reserved.
//

#import "WWAppDelegate.h"

extern WIP7Spec							*WCP7Spec;

@implementation WWAppDelegate

- (void)dealloc
{
    [super dealloc];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    WIError *error = nil;
    NSString *path = [[NSBundle mainBundle] pathForResource:@"wired" ofType:@"xml"];
    
    // init and load the Wired 2.0 P7 Specification
	WCP7Spec = [[WIP7Spec alloc] initWithPath:path originator:WIP7Client error:&error];
	if(!WCP7Spec) {		
		[NSApp terminate:self];
	}
    
    WIP7Message *message = [WIP7Message messageWithName:@"wired.send_login" spec:WCP7Spec];
    
    NSLog(@"message: %@", message);
}

@end
