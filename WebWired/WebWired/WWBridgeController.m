//
//  WWWiredBridge.m
//  WebWired
//
//  Created by Rafaël Warnault on 16/02/13.
//  Copyright (c) 2013 read-write.fr. All rights reserved.
//

#import "WWBridgeController.h"

static WWBridgeController *_instance;

@implementation WWBridgeController


#pragma mark -

+ (id)sharedController {
    if(!_instance) {
        _instance = [[[self class] alloc] init];
    }
    return _instance;
}



#pragma mark -

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

@end
