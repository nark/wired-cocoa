//
//  WWWiredBridge.h
//  WebWired
//
//  Created by Rafaël Warnault on 16/02/13.
//  Copyright (c) 2013 read-write.fr. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WWBridgeController : NSObject

+ (id)sharedController;

//- (void)

@end
