//
//  main.m
//  WebWired
//
//  Created by Rafaël Warnault on 16/02/13.
//  Copyright (c) 2013 read-write.fr. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
