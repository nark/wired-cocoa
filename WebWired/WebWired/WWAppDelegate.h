//
//  WWAppDelegate.h
//  WebWired
//
//  Created by Rafaël Warnault on 16/02/13.
//  Copyright (c) 2013 read-write.fr. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface WWAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
