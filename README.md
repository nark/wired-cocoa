# Wired Readme

Originally, Wired was developed by Axel Andersson (aka Morris) at [Zanka Software](http://zankasoftware.com/), as a substitute to the old  bulletin-board system named Hotline which be massively used in the 1990s. This project is a fork of the original Zanka sources, or maybe a continuation, since Morris stopped its development.

Project was cleaned and was made compliant with Xcode 4. It provides libraries and frameworks to make it easy to build programs which use the Wired protocol 2.0. The choose of this version of the protocol was explained in a post I wrote [here](http://blog.read-write.fr/blog/2011/12/01/wired-disaster/). 

The project is currently beta. You must know that the protocol version used here (2.0b55) is not compliant with [versions distributed at Zanka Software](http://zankasoftware.com/nightly/wired-2.0/) (2.0b51), because of inconsistencies in the original source code. 

### Components :

#### Programs:

- **wired:** UNIX-based Wired server
- **wire:** UNIX-based Wired client in command line
- **wirebot** UNIX-based Wired chat-bot
- **Wired Client:** Client application for Mac OS X
- **Wired Server:** Server application for Mac OS X
- **WireBot Manager:** Chat-bot application for Mac OS X

#### Libraries:

- **libwired:** Core engine for the Wired network suite (written in C). It contains collections and other data structures, and portable abstractions for many OS services, like threads, sockets, files, etc
- **WiredFoundation:** Objective-C framework for Wired support
- **WiredNetworking:** Objective-C framework that encapsulates the Wired network protocol
- **WiredAppKit:** Objective-C framework that contains extensions and classes build against AppKit framework
- **WiredData:** Objective-C framework providing a Core Data support for Wired objects
- **WiredKit:** Objective-C a hight-level API for Wired
- **WiredExampleMac:** A very basic example of using WiredKit.
 
### Sources

Sources are available at Bitbucket : [https://bitbucket.org/nark/wired/](https://bitbucket.org/nark/wired/)

You are welcome to join the project as a contributor, a developer, a translator or a tester. If you need to work directly on sources, I recommend you to fork the project using your own Bitbucket repository. Follow this [guide](https://bitbucket.org/nark/wired/wiki/How_to_contribute_to_wired_project).

### Downloads

You can download compiled binaries on the page : [http://www.read-write.fr/wired/index.php?page=1](http://www.read-write.fr/wired/index.php?page=1). They are beta for the moment, work in progress...


### How-to's

You can found some assistance on the Wired Wiki here : [https://bitbucket.org/nark/wired/wiki](https://bitbucket.org/nark/wired/wiki)


### License

This code is distributed under BSD license, and it is free for personal or commercial use.

Copyright (c) 2003-2009 Axel Andersson
Copyright (c) 2011 Rafaël Warnault
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
