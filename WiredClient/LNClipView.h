//
//  LNClipView.h
//  LinenClipView
//
//  Created by David Keegan on 10/22/11.
//  Copyright (c) David Keegan. All rights reserved.
//

#import <AppKit/AppKit.h>
#import <WebKit/WebKit.h>

@class LNClipView, LNWebClipView;

@interface LNScrollView : NSScrollView {
	 LNClipView *_clipView;
}
- (void)setPattern:(NSImage *)pattern;
@end

@interface LNWebView : WebView {
	LNWebClipView *_clipView;	
}

- (void)setPattern:(NSImage *)pattern;
@end
