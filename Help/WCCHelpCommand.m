//
//  WCCHelpCommand.m
//  wired
//
//  Created by Rafaël Warnault on 19/06/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WCCHelpCommand.h"

@implementation WCCHelpCommand




#pragma mark -

- (void)initPlugin {
	_command		= [@"/help" retain];
	_hasArguments	= NO;
}




#pragma mark -

- (NSString *)commandOutput {

}

@end
