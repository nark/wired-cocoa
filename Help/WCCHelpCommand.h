//
//  WCCHelpCommand.h
//  wired
//
//  Created by Rafaël Warnault on 19/06/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WIChatPlugin.h"

@interface WCCHelpCommand : WIChatPlugin

@end
