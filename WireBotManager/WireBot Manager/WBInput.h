//
//  WBInput.h
//  WireBot Manager
//
//  Created by Rafaël Warnault on 31/05/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBValueObject.h"


enum WBInputComparisonMethod {
	WBInputComparisonEquals		= 0,
	WBInputComparisonContains	= 1,
	WBInputComparisonStarts		= 2,
	WBInputComparisonEnds		= 3
} typedef WBInputComparisonMethod;


@interface WBInput : WBValueObject {
	WBInputComparisonMethod		_comparison;
	BOOL						_caseSensitive;
}

+ (WBInputComparisonMethod)comparisonMethodForString:(NSString *)string;
+ (NSString *)stringForComparisonMethod:(WBInputComparisonMethod)method;

- (WBInputComparisonMethod)comparison;
- (void)setComparison:(WBInputComparisonMethod)comparison;

- (BOOL)isCaseSensitive;
- (void)setCaseSensitive:(BOOL)sensitive;

@end
