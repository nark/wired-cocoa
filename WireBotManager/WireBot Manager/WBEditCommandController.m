//
//  WBEditCommandController.m
//  WireBot Manager
//
//  Created by Rafaël Warnault on 02/06/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBEditCommandController.h"
#import "WBCommand.h"
#import "WBOutput.h"





@interface WBEditCommandController (Private)

- (void)_validate;

- (void)_clearSheet;
- (BOOL)_isSheetValid;

- (WBOutput *)_selectedOutput;

@end







@implementation WBEditCommandController (Private)


- (void)_validate {
	[_removeOutputButton setEnabled:([self _selectedOutput] != nil)];
}


- (void)_clearSheet {
	[_activatedButton setState:NSOffState];
	[_permissionsTextField setStringValue:@""];
	[_nameTextField setStringValue:@""];
	[_nameTextField setEnabled:YES];
	
	[_outputs removeAllObjects];
	
	[_editingCommand release], _editingCommand = nil;
}


- (BOOL)_isSheetValid {
	return YES;
}


#pragma mark -

- (WBOutput *)_selectedOutput {
	WBOutput	*selectedOutput;
	NSInteger	selectedRow;
	
	selectedOutput	= nil;
	selectedRow		= [_outputsTableView selectedRow];
	
	if(selectedRow != -1)
		selectedOutput	= [_outputs objectAtIndex:selectedRow];
	
	return selectedOutput;
}


@end








@implementation WBEditCommandController


#pragma mark -

@synthesize editingCommand		= _editingCommand;
@synthesize outputs				= _outputs;




#pragma mark -

- (id)init
{
    self = [super init];
    if (self) {
        _outputs = [[NSMutableArray alloc] init];
    }
    return self;
}


- (void)dealloc
{
    [_editingCommand release];
	[_outputs release];
	
    [super dealloc];
}




#pragma mark -


- (IBAction)addOutput:(id)sender {
	WBOutput *newOutput;
	
	newOutput = [[WBOutput alloc] init];
	
	[newOutput setContentValue:@":-)"];
	[newOutput setMessage:@"wired.chat.say"];
	[newOutput setRepeat:0];
	[newOutput setDelay:0];
	
	[_outputs addObject:newOutput];
	[_outputsTableView reloadData];
}


- (IBAction)removeOutput:(id)sender {
	WBOutput *selectedOutput;
	
	selectedOutput = [self _selectedOutput];
	
	if(selectedOutput)
		[_outputs removeObject:selectedOutput];
	
	[_outputsTableView reloadData];
}






#pragma mark -

- (void)openSheet:(id)sender {
	
	if(_editingCommand) {
		[_activatedButton setState:[_editingCommand isActivated]];
		[_nameTextField setStringValue:[_editingCommand name]];
		[_permissionsTextField setStringValue:[_editingCommand permissions]];
		
		[_outputs addObjectsFromArray:[_editingCommand outputs]];
		
		[_nameTextField setEnabled:![_editingCommand isPrivateCommand]];
	}
	
	[_outputsTableView reloadData];
	
	[self _validate];
	
	[super openSheet:sender];
}

- (void)cancelSheet:(id)sender {
	[super cancelSheet:sender];
	[self _clearSheet];
}


- (void)messageDelegateAndCloseSheet:(id)sender {
	
	if(!_editingCommand)
		_editingCommand = [[WBCommand alloc] init];
	
	[_editingCommand setName:[_nameTextField stringValue]];
	[_editingCommand setActivated:[_activatedButton state]];
	[_editingCommand setPermissions:[_permissionsTextField stringValue]];
	[_editingCommand setOutputs:_outputs];
	
	[super messageDelegateAndCloseSheet:_editingCommand];
	[self _clearSheet];
}





#pragma mark -


- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
	if(tableView == _outputsTableView) {
		return [_outputs count];
	}
	return 0;
}

- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
	if(tableView == _outputsTableView) {
		
		if(!_outputs || [_outputs count] == 0)
			return nil;
		
		if([[tableColumn identifier] isEqualToString:@"value"])
			return [[_outputs objectAtIndex:row] contentValue];
		
		else if([[tableColumn identifier] isEqualToString:@"message"])
			return [NSNumber numberWithInteger:[WBOutput messageTypeForWiredMessage:[[_outputs objectAtIndex:row] message]]];
		
		else if([[tableColumn identifier] isEqualToString:@"delay"])
			return [NSNumber numberWithInteger:[[_outputs objectAtIndex:row] delay]];
		
		else if([[tableColumn identifier] isEqualToString:@"repeat"])
			return [NSNumber numberWithBool:[[_outputs objectAtIndex:row] repeat]];
	}
	return nil;
}


- (void)tableView:(NSTableView *)tableView setObjectValue:(id)object forTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
	if(tableView == _outputsTableView) {
		if([[tableColumn identifier] isEqualToString:@"value"])
			[[_outputs objectAtIndex:row] setContentValue:object];
		
		else if([[tableColumn identifier] isEqualToString:@"message"])
			[[_outputs objectAtIndex:row] setMessage:[WBOutput wiredMessageForMessageType:[object integerValue]]];
		
		else if([[tableColumn identifier] isEqualToString:@"delay"])
			[[_outputs objectAtIndex:row] setDelay:[object integerValue]];
		
		else if([[tableColumn identifier] isEqualToString:@"repeat"])
			[[_outputs objectAtIndex:row] setRepeat:[object integerValue]];
	}
	
	[self _validate];
}

- (void)tableViewSelectionDidChange:(NSNotification *)notification {
	[self _validate];
}



@end
