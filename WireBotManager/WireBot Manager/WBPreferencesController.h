//
//  WBPreferencesController.h
//  WireBot Manager
//
//  Created by Rafaël Warnault on 03/06/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import <Foundation/Foundation.h>



extern NSString * const		WBSaveAutoReloadWirebot;
extern NSString * const		WBQuitRememberUserChoice;
extern NSString * const		WBQuitSavedUserChoiceType;



enum WBQuitUserChoiceType {
	WBQuitUserChoiceAskMe		= 0,
	WBQuitUserChoiceQuit		= 1,
	WBQuitUserChoiceQuitAll		= 2
} typedef WBQuitUserChoiceType;




@interface WBPreferencesController : NSWindowController {
	NSMatrix		*_quitOptionsMatrix;
}

@property (assign)		IBOutlet NSMatrix		*quitOptionsMatrix;

- (IBAction)saveOptionChange:(id)sender;

@end
