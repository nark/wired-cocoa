//
//  WBCommandsController.h
//  WireBot Manager
//
//  Created by Rafaël Warnault on 31/05/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBCollectionController.h"

@class WBCommand, WBOutput;

@interface WBCommandsController : WBCollectionController {
	WBCommand	*_currentCommand;
	WBOutput	*_currentOutput;
}

@end
