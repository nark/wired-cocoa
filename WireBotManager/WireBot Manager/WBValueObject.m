//
//  WBResponseObject.m
//  WireBot Manager
//
//  Created by Rafaël Warnault on 31/05/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBValueObject.h"

@implementation WBValueObject



#pragma mark -

+ (NSString *)wiredMessageForMessageType:(WBWiredMessageType)type {
	switch (type) {
		case WBWiredMessageChatSayMessage:		return @"wired.chat.say";			break;
		case WBWiredMessageChatMeMessage:		return @"wired.chat.me";			break;
		case WBWiredMessagePrivateMessage:		return @"wired.message.message";	break;
		case WBWiredMessageBroadcastMessage:	return @"wired.message.broadcast";	break;
		case WBWiredMessageUserJoindMessage:	return @"wired.chat.user_join";		break;
		case WBWiredMessageUserLeaveMessage:	return @"wired.chat.user_leave";	break;
		default:								return @"wired.chat.say";			break;
	}
}


+ (WBWiredMessageType)messageTypeForWiredMessage:(NSString *)message {
	if([message isEqualToString:@"wired.chat.say"]) {
		return WBWiredMessageChatSayMessage;
		
	} else if([message isEqualToString:@"wired.chat.me"]) {
		return WBWiredMessageChatMeMessage;
		
	} else if([message isEqualToString:@"wired.message.message"]) {
		return WBWiredMessagePrivateMessage;
		
	} else if([message isEqualToString:@"wired.message.broadcast"]) {
		return WBWiredMessageBroadcastMessage;
		
	} else if([message isEqualToString:@"wired.chat.user_join"]) {
		return WBWiredMessageUserJoindMessage;
		
	} else if([message isEqualToString:@"wired.chat.user_leave"]) {
		return WBWiredMessageUserLeaveMessage;
		
	} else {
		return WBWiredMessageChatSayMessage;
	}
}




#pragma mark -

- (void)dealloc
{
    [_value release];
	[_message release];
    [super dealloc];
}




#pragma mark -

- (NSString *)contentValue {
	return _value;
}

- (void)setContentValue:(NSString *)value {
	if(_value)
		[_value release], _value = nil;
	
	_value = [value retain];
}



- (NSString *)message {
	return _message;
}

- (void)setMessage:(NSString *)message {
	if(_message)
		[_message release], _message = nil;
	
	_message = [message retain];
}



@end
