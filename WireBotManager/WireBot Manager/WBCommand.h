//
//  WBCommand.h
//  WireBot Manager
//
//  Created by Rafaël Warnault on 31/05/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBTriggerObject.h"

@class WBOutput;

@interface WBCommand : WBTriggerObject {
	NSMutableArray		*_outputs;
	NSString			*_name;
}

+ (NSArray *)privateCommandNames;

- (NSString *)name;
- (void)setName:(NSString *)name;

- (NSArray *)outputs;
- (void)setOutputs:(NSArray *)outputs;

- (void)addOutput:(WBOutput *)output;
- (void)removeOutput:(WBOutput *)output;

- (BOOL)isPrivateCommand;

- (NSString *)outputsString;

@end
