//
//  WBEditCommandController.h
//  WireBot Manager
//
//  Created by Rafaël Warnault on 02/06/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBSheetController.h"

@class WBCommand, WBOutput;

@interface WBEditCommandController : WBSheetController {
	
	IBOutlet NSButton			*_activatedButton;
	
	IBOutlet NSTextField		*_nameTextField;
	IBOutlet NSTextField		*_permissionsTextField;
	
	IBOutlet NSTableView		*_outputsTableView;
	IBOutlet NSButton			*_removeOutputButton;
	
	WBCommand					*_editingCommand;
	NSMutableArray				*_outputs;
}
	
@property (readwrite, retain)	WBCommand			*editingCommand;
@property (readwrite, retain)	NSMutableArray		*outputs;

- (IBAction)addOutput:(id)sender;
- (IBAction)removeOutput:(id)sender;

@end
