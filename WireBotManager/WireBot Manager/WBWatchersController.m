
//
//  WBWatchersController.m
//  WireBot Manager
//
//  Created by Rafaël Warnault on 31/05/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBWatchersController.h"
#import "WBWatcher.h"

@implementation WBWatchersController



#pragma mark -

- (void)dealloc
{
    [_currentWatcher release];
    [super dealloc];
}



#pragma mark -

- (NSString *)XMLRepresentation {
	NSMutableString *string;
	
	string = [NSMutableString string];
	
	[string appendString:@"\t<watchers>\n"];
	
	for(WBWatcher *watcher in _objects) {
		[string appendString:[watcher XMLRepresentation]];
	}
	
	[string appendString:@"\t</watchers>\n\n"];
	
	return string;
}

- (NSData *)XMLData {
	NSMutableData *XMLData = [NSMutableData data];
	
	for(WBWatcher *watcher in _objects) {
		[XMLData appendData:[watcher XMLData]];
	}
	
	return XMLData;
}




#pragma mark -

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{	
	if([elementName isEqualToString:_key]) {
		if(!_currentWatcher) {
			_currentWatcher = [[WBWatcher alloc] init];
			
			if([attributeDict valueForKey:@"path"])
				[_currentWatcher setPath:[attributeDict valueForKey:@"path"]];
			
			if([attributeDict valueForKey:@"activated"])
				[_currentWatcher setActivated:[attributeDict boolForKey:@"activated"]];
			
			return;
		}
	}
}


- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName  {
	if([elementName isEqualToString:_key]) {
		
		[_objects addObject:_currentWatcher];
		
		if(_currentWatcher)
			[_currentWatcher release], _currentWatcher = nil;
		
		return;
	}
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string  {	

}



@end
