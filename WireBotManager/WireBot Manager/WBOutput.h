//
//  WBOutput.h
//  WireBot Manager
//
//  Created by Rafaël Warnault on 31/05/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBValueObject.h"

@interface WBOutput : WBValueObject {
	NSInteger		_delay;
	NSInteger		_repeat;
}

- (NSInteger)delay;
- (void)setDelay:(NSInteger)delay;

- (NSInteger)repeat;
- (void)setRepeat:(NSInteger)repeat;

@end
