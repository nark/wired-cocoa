//
//  WBRule.h
//  WireBot Manager
//
//  Created by Rafaël Warnault on 30/05/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBTriggerObject.h"

@class WBInput, WBOutput;

@interface WBRule : WBTriggerObject {
	NSMutableArray		*_inputs;
	NSMutableArray		*_outputs;
}

- (NSArray *)inputs;
- (void)setInputs:(NSArray *)inputs;

- (void)addInput:(WBInput *)input;
- (void)removeInput:(WBInput *)input;


- (NSArray *)outputs;
- (void)setOutputs:(NSArray *)outputs;

- (void)addOutput:(WBOutput *)output;
- (void)removeOutput:(WBOutput *)output;


- (NSString *)inputsString;
- (NSString *)outputsString;

@end
