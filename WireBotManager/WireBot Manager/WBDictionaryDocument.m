//
//  WBDocument.m
//  WireBot Manager
//
//  Created by Rafaël Warnault on 05/04/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBDictionaryDocument.h"
#import "WBRulesController.h"
#import "WBCommandsController.h"
#import "WBWatchersController.h"
#import "WBEditRuleController.h"
#import "WBEditCommandController.h"
#import "WBRule.h"
#import "WBCommand.h"





@interface WBDictionaryDocument (Private)

- (void)_validate;

- (void)_reloadData;
- (void)_reloadXML;

- (WBRule *)_selectedRule;
- (WBCommand *)_selectedCommand;
- (WBWatcher *)_selectedWatcher;

- (WBXMLObject *)_selectedObjectInArray:(NSArray *)array forTableView:(NSTableView *)tableView;
 
@end




@implementation WBDictionaryDocument (Private)


#pragma mark -

- (void)_validate {
	[self.removeRuleButton setEnabled:([self.rulesTableView selectedRow] != -1)];
}




#pragma mark -

- (WBRule *)_selectedRule {
	return (WBRule *)[self _selectedObjectInArray:[_rulesController allObjects] 
									 forTableView:_rulesTableView];
}

- (WBCommand *)_selectedCommand {
	return (WBCommand *)[self _selectedObjectInArray:[_commandsController allObjects] 
										forTableView:_commandsTableView];
}

- (WBWatcher *)_selectedWatcher {
	return (WBWatcher *)[self _selectedObjectInArray:[_watchersController allObjects] 
										forTableView:_watchersTableView];
}


- (WBXMLObject *)_selectedObjectInArray:(NSArray *)array forTableView:(NSTableView *)tableView {
	WBXMLObject		*selected;
	NSInteger		selectedRow;
	
	selected	= nil;
	selectedRow		= [tableView selectedRow];
	
	if(selectedRow != -1)
		selected	= [array objectAtIndex:selectedRow];
	
	return selected;
}




#pragma mark -

- (void)_reloadData {	
	[_commandsTableView reloadData];
	[_rulesTableView reloadData];
	[_watchersTableView reloadData];
}


- (void)_reloadXML {

	NSFont *font = [NSFont fontWithName:@"Menlo" size:11.0f]; 
	NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName]; 
	
	NSAttributedString *string = [[NSAttributedString alloc] initWithString:[self XMLRepresentation] attributes:stringAttributes];
	
	[[_xmlTextView textStorage] setAttributedString:string];
	
	[string release];
}


@end






@implementation WBDictionaryDocument



#pragma mark -

@synthesize toolbar					= _toolbar;
@synthesize tabView					= _tabView;

@synthesize rulesTableView			= _rulesTableView;
@synthesize commandsTableView		= _commandsTableView;
@synthesize watchersTableView		= _watchersTableView;

@synthesize addRuleButton			= _addRuleButton;
@synthesize removeRuleButton		= _removeRuleButton;
@synthesize rulePopUpButton			= _rulePopUpButton;

@synthesize xmlTextView				= _xmlTextView;

@synthesize editRuleController		= _editRuleController;
@synthesize editCommandController	= _editCommandController;

@synthesize rulesController			= _rulesController;
@synthesize commandsController		= _commandsController;
@synthesize watchersController		= _watchersController;





#pragma mark -

- (id)init {
    self = [super init];
    if (self) {
		
		_rulesController	= [[WBRulesController alloc] initForKey:@"rule"];
		_commandsController = [[WBCommandsController alloc] initForKey:@"command"];
		_watchersController = [[WBWatchersController alloc] initForKey:@"watcher"];
    }
    return self;
}


- (void)dealloc
{
    [_rulesController release];
	[_commandsController release];
	[_watchersController release];
    [super dealloc];
}





#pragma mark -

- (NSString *)windowNibName {
    return @"WBDictionaryDocument";
}


- (void)windowControllerDidLoadNib:(NSWindowController *)aController
{
    [super windowControllerDidLoadNib:aController];
  
	// select default item in the toolbar
	[_toolbar setSelectedItemIdentifier:@"Rules"];
	
	// set tables double actions
	[_rulesTableView setDoubleAction:@selector(editRule:)];
	[_commandsTableView setDoubleAction:@selector(editCommand:)];
	
	// non-wrap the XML text view
	[[_xmlTextView textContainer] setContainerSize:NSMakeSize(FLT_MAX, FLT_MAX)];
	[[_xmlTextView textContainer] setWidthTracksTextView:NO];
	[_xmlTextView setHorizontallyResizable:YES];
	
	// XML text view text attributes
	[[_xmlTextView textStorage] setFont:[NSFont fontWithName:@"Menlo" size:11]];
	
	// reload data
	[self _reloadData];
	[self _reloadXML];
	
	// validate user controls
	[self _validate];
}


- (NSData *)dataOfType:(NSString *)typeName error:(NSError **)outError
{
    return [self XMLData];
}


- (BOOL)readFromData:(NSData *)data ofType:(NSString *)typeName error:(NSError **)outError
{	
	[_rulesController setData:data];
	[_commandsController setData:data];
	[_watchersController setData:data];
	
	[self _reloadData];
	
    return YES;
}





#pragma mark -

- (IBAction)switchView:(id)sender {
	NSInteger tag;
	
	tag = [sender tag];
	
	[_tabView selectTabViewItemAtIndex:tag];
}



- (IBAction)addRule:(id)sender {
	[self.editRuleController openSheet:sender];
}


- (IBAction)editRule:(id)sender {
	WBRule		*selectedRule;
	
	selectedRule	= [self _selectedRule];

	if(selectedRule) {
		[self.editRuleController setEditingRule:selectedRule];
		[self.editRuleController openSheet:sender];
	}
}


- (IBAction)removeRule:(id)sender {
	WBRule		*selectedRule;
	
	selectedRule	= [self _selectedRule];
	
	if(selectedRule) {
		NSAlert *alert = [NSAlert alertWithMessageText:@"Removing Rule" 
										 defaultButton:@"Remove" 
									   alternateButton:@"Cancel" 
										   otherButton:nil 
							 informativeTextWithFormat:@"Are you sure you want to remove this rule ? This operation is not cancellable."];
		
		[alert beginSheetModalForWindow:[[[self windowControllers] objectAtIndex:0] window]
						  modalDelegate:self
						 didEndSelector:@selector(removeRuleAlertDidEnd:returnCode:contextInfo:)
							contextInfo:selectedRule];
	}
}

- (void)removeRuleAlertDidEnd:(NSAlert *)alert returnCode:(NSInteger)returnCode contextInfo:(void *)contextInfo {
	WBRule		*selectedRule;
	
	if(returnCode == NSOKButton) {
		selectedRule = (WBRule *)contextInfo;
		
		if(selectedRule) {
			[_rulesController removeObject:selectedRule];
			
			[_rulesTableView reloadData];
			[self _reloadXML];
			
			[self updateChangeCount:NSChangeDone];
		}
	}
}


- (IBAction)duplicateRule:(id)sender {
	WBRule		*selectedRule;
	WBRule		*duplicatedRule;
	
	selectedRule	= [self _selectedRule];
	
	if(selectedRule) {
		duplicatedRule = [selectedRule copy];
		
		[duplicatedRule setActivated:NO];
		
		[_rulesController addObject:duplicatedRule];
		
		[_rulesTableView reloadData];
		[self _reloadXML];
	}
}




#pragma mark -

- (IBAction)addCommand:(id)sender {
	[self.editCommandController openSheet:sender];
}


- (IBAction)editCommand:(id)sender {
	WBCommand		*selectedCommand;
	
	selectedCommand	= [self _selectedCommand];
	
	if(selectedCommand) {
		[self.editCommandController setEditingCommand:selectedCommand];
		[self.editCommandController openSheet:sender];
	}
}


- (IBAction)removeCommand:(id)sender {
	NSAlert			*alert;
	WBCommand		*selectedCommand;
	
	selectedCommand	= [self _selectedCommand];
	
	if(selectedCommand) {
		if([selectedCommand isPrivateCommand]) {
			alert = [NSAlert alertWithMessageText:@"Private Command" 
									defaultButton:@"OK" 
								  alternateButton:nil 
									  otherButton:nil 
						informativeTextWithFormat:[NSString stringWithFormat:@"The command \"%@\" is a private internal command of Wirebot. You cannot delete it."], [selectedCommand name]];
			
			[alert beginSheetModalForWindow:[[[self windowControllers] objectAtIndex:0] window]];
			return;
			
		} else {
			alert = [NSAlert alertWithMessageText:@"Removing Rule" 
									defaultButton:@"Remove" 
								  alternateButton:@"Cancel" 
									  otherButton:nil 
						informativeTextWithFormat:@"Are you sure you want to remove this rule ? This operation is not cancellable."];
			
			[alert beginSheetModalForWindow:[[[self windowControllers] objectAtIndex:0] window]
							  modalDelegate:self
							 didEndSelector:@selector(removeCommandAlertDidEnd:returnCode:contextInfo:)
								contextInfo:selectedCommand];
		}
	}
}

- (void)removeCommandAlertDidEnd:(NSAlert *)alert returnCode:(NSInteger)returnCode contextInfo:(void *)contextInfo {
	WBCommand		*selectedCommand;
	
	if(returnCode == NSOKButton) {
		selectedCommand = (WBCommand *)contextInfo;
		
		if(selectedCommand) {
			[_commandsController removeObject:selectedCommand];
			
			[_commandsTableView reloadData];
			[self _reloadXML];
			
			[self updateChangeCount:NSChangeDone];
		}
	}
}



- (IBAction)duplicateCommand:(id)sender {
	WBCommand		*selectedCommand;
	WBCommand		*duplicatedCommand;
	
	selectedCommand	= [self _selectedCommand];
	
	if(selectedCommand && ![selectedCommand isPrivateCommand]) {
		duplicatedCommand = [selectedCommand copy];
		[duplicatedCommand setActivated:NO];
		
		[_commandsController addObject:duplicatedCommand];
		
		[_commandsTableView reloadData];
		[self _reloadXML];
	}
}





#pragma mark -

- (void)sheetController:(WBSheetController *)controller shouldCloseSheet:(id)sender {
	if(controller == self.editRuleController) {
		if(![_rulesController containsObject:sender])
			[_rulesController addObject:sender];
		
		[self updateChangeCount:NSChangeDone];
		
		[self _reloadXML];
		[_rulesTableView reloadData];
	}
	else if(controller == self.editCommandController) {
		if(![_commandsController containsObject:sender])
			[_commandsController addObject:sender];
		
		[self updateChangeCount:NSChangeDone];
		
		[self _reloadXML];
		[_commandsTableView reloadData];
	}
}





#pragma mark -

- (NSString *)XMLRepresentation {
	NSMutableString *string;
	
	string = [NSMutableString string];
	
	[string appendString:@"<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<wirebot>\n"];
	
	if([_rulesController isEmpty])
		[string appendString:[_rulesController XMLRepresentation]];
	
	if([_commandsController isEmpty])
		[string appendString:[_commandsController XMLRepresentation]];
	
	if([_watchersController isEmpty])
		[string appendString:[_watchersController XMLRepresentation]];
	
	[string appendString:@"</wirebot>"];
	
	return string;
}

- (NSData *)XMLData {
	return [[self XMLRepresentation] dataUsingEncoding:NSUTF8StringEncoding];
}





#pragma mark -

- (BOOL)validateMenuItem:(NSMenuItem *)menuItem {
	SEL action;
	
	action = [menuItem action];
	
	if(action == @selector(removeRule:))
		return ([self _selectedRule] != nil);
	
	else if(action == @selector(editRule:))
		return ([self _selectedRule] != nil);
	
	else if(action == @selector(duplicateRule:))
		return ([self _selectedRule] != nil);
	
	else if(action == @selector(removeCommand:))
		return ([self _selectedCommand] != nil);
	
	else if(action == @selector(editCommand:))
		return ([self _selectedCommand] != nil);
	
	else if(action == @selector(duplicateCommand:))
		return ([self _selectedCommand] != nil);
	
	return YES;
}




#pragma mark -

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
	if(_rulesTableView == tableView) {
		return [_rulesController numberOfObjects];
		
	} else if(_commandsTableView == tableView) {
		return [_commandsController numberOfObjects];
		
	} else if(_watchersTableView == tableView) {
		return [_watchersController numberOfObjects];
	}
	return 0;
}


- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {	
	id value = nil;
	
	if(tableView == _rulesTableView) {
		if([[tableColumn identifier] isEqualToString:@"activated"])
			value = [NSNumber numberWithBool:[[_rulesController objectAtIndex:row] isActivated]];
		
		if([[tableColumn identifier] isEqualToString:@"permissions"]) 
			value = [[_rulesController objectAtIndex:row] permissions];
		
		if([[tableColumn identifier] isEqualToString:@"inputs"]) 
			value = [[_rulesController objectAtIndex:row] inputsString];
		
		if([[tableColumn identifier] isEqualToString:@"outputs"]) 
			value = [[_rulesController objectAtIndex:row] outputsString];
	} 
	else if(tableView == _commandsTableView) {
		if([[tableColumn identifier] isEqualToString:@"activated"])
			value = [NSNumber numberWithBool:[[_commandsController objectAtIndex:row] isActivated]];
		
		if([[tableColumn identifier] isEqualToString:@"name"])
			value = [[_commandsController objectAtIndex:row] name];
		
		if([[tableColumn identifier] isEqualToString:@"permissions"])
			value = [[_commandsController objectAtIndex:row] permissions];
		
		if([[tableColumn identifier] isEqualToString:@"outputs"])
			value = [[_commandsController objectAtIndex:row] outputsString];
	}
	else if(tableView == _watchersTableView) {
		if([[tableColumn identifier] isEqualToString:@"activated"])
			value = [NSNumber numberWithBool:[[_watchersController objectAtIndex:row] isActivated]];
		
		if([[tableColumn identifier] isEqualToString:@"path"])
			value = [[_watchersController objectAtIndex:row] path];
	}
	return value;
}


- (void)tableView:(NSTableView *)tableView setObjectValue:(id)object forTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row{
	if(tableView == _rulesTableView) {
		if([[tableColumn identifier] isEqualToString:@"activated"])
			[[_rulesController objectAtIndex:row] setActivated:[object boolValue]];
			
		if([[tableColumn identifier] isEqualToString:@"permissions"]) 
			[[_rulesController objectAtIndex:row] setPermissions:object];
		
	} else if(tableView == _commandsTableView) {
		if([[tableColumn identifier] isEqualToString:@"activated"])
			[[_commandsController objectAtIndex:row] setActivated:[object boolValue]];
		
		if([[tableColumn identifier] isEqualToString:@"permissions"]) 
			[[_commandsController objectAtIndex:row] setPermissions:object];
		
	} else if(tableView == _watchersTableView) {
		if([[tableColumn identifier] isEqualToString:@"activated"])
			[[_watchersController objectAtIndex:row] setActivated:[object boolValue]];
		
		if([[tableColumn identifier] isEqualToString:@"path"]) 
			[[_watchersController objectAtIndex:row] setPath:object];
	}
	
	[self updateChangeCount:NSChangeDone];
	[self _reloadXML];
}


- (void)tableViewSelectionDidChange:(NSNotification *)notification {
	[self _validate];
}

@end
