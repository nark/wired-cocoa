//
//  WBRule.m
//  WireBot Manager
//
//  Created by Rafaël Warnault on 30/05/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBRule.h"
#import "WBInput.h"
#import "WBOutput.h"


@implementation WBRule



#pragma mark -

- (id)init
{
    self = [super init];
    if (self) {
        _inputs		= [[NSMutableArray alloc] init];
        _outputs	= [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc
{
    [_inputs release];
	[_outputs release];
    [super dealloc];
}



#pragma mark -

- (id)copyWithZone:(NSZone *)zone {
	WBRule		*rule;
	
	rule = [[[self class] allocWithZone:zone] init];
	
	[rule setPermissions:[self permissions]];
	[rule setActivated:[self isActivated]];
	[rule setInputs:[self inputs]];
	[rule setOutputs:[self outputs]];
	
	return rule;
}




#pragma mark -

- (NSArray *)inputs {
	return _inputs;
}

- (void)setInputs:(NSArray *)inputs {
	[_inputs setArray:inputs];
}

- (void)addInput:(WBInput *)input {
	[_inputs addObject:input];
}

- (void)removeInput:(WBInput *)input {
	[_inputs removeObject:input];
}



- (NSArray *)outputs {
	return _outputs;
}

- (void)setOutputs:(NSArray *)outputs {
	[_outputs setArray:outputs];
}

- (void)addOutput:(WBOutput *)output {
	[_outputs addObject:output];
}

- (void)removeOutput:(WBOutput *)output {
	[_outputs removeObject:output];
}



#pragma mark -

- (NSString *)inputsString {
	NSMutableString *string;
	
	string = [NSMutableString string];
	
	for(NSInteger i = 0; i < [[self inputs] count]; i++) {
		if([[[self inputs] objectAtIndex:i] contentValue]) {
			[string appendString:[[[self inputs] objectAtIndex:i] contentValue]];
		
		if(i != [[self inputs] count]-1)
			[string appendString:@", "];
		}
	}
	
	return string;
}

- (NSString *)outputsString {
	NSMutableString *string;
	
	string = [NSMutableString string];
	
	for(NSInteger i = 0; i < [[self outputs] count]; i++) {
		if([[[self outputs] objectAtIndex:i] contentValue]) {
			[string appendString:[[[self outputs] objectAtIndex:i] contentValue]];
		
		if(i != [[self outputs] count]-1)
			[string appendString:@", "];
		}
	}
	
	return string;
}



#pragma mark -

- (NSString *)XMLRepresentation {
	NSMutableString *string;
	
	string = [NSMutableString string];
	
	[string appendFormat:@"\t\t<rule permissions=\"%@\" activated=\"%@\">\n", 
						(_permissions)	? _permissions : @"any", 
						(_activated)	? @"true" : @"false"];
	
	for(NSInteger i = 0; i < [[self inputs] count]; i++) {
		[string appendFormat:[[[self inputs] objectAtIndex:i] XMLRepresentation]];
		[string appendString:@"\n"];
	}
	
	[string appendString:@"\n"];
	
	for(NSInteger i = 0; i < [[self outputs] count]; i++) {
		[string appendFormat:[[[self outputs] objectAtIndex:i] XMLRepresentation]];
		[string appendString:@"\n"];
	}
	
	[string appendString:@"\t\t</rule>\n\n"];
	
	return string;
}

- (NSData *)XMLData {
	return [[self XMLRepresentation] dataUsingEncoding:NSUTF8StringEncoding];
}


@end
