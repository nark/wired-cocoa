//
//  WBSheetController.h
//  WireBot Manager
//
//  Created by Brandon Walkin (www.brandonwalkin.com)
//  All code is provided under the New BSD license.
//

#import <Cocoa/Cocoa.h>


@protocol WBSheetControllerDelegate;

@interface WBSheetController : NSObject
{
	NSWindow						*sheet;
	NSWindow						*parentWindow;
	id<WBSheetControllerDelegate>	delegate;
}

@property (nonatomic, retain) IBOutlet NSWindow *sheet, *parentWindow;
@property (nonatomic, retain) IBOutlet id<WBSheetControllerDelegate> delegate;

- (IBAction)openSheet:(id)sender;
- (IBAction)closeSheet:(id)sender;
- (IBAction)cancelSheet:(id)sender;
- (IBAction)messageDelegateAndCloseSheet:(id)sender;

@end



@protocol WBSheetControllerDelegate <NSObject>

- (void)sheetController:(WBSheetController *)controller shouldCloseSheet:(id)sender;

@end