//
//  WBPreferencesController.m
//  WireBot Manager
//
//  Created by Rafaël Warnault on 03/06/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBPreferencesController.h"



NSString * const WBSaveAutoReloadWirebot		= @"WBSaveAutoReloadWirebot";
NSString * const WBQuitRememberUserChoice		= @"WBQuitRememberUserChoice";
NSString * const WBQuitSavedUserChoiceType		= @"WBQuitSavedUserChoiceType";





@interface WBPreferencesController (Private)
- (void)_reloadSettings;
@end



@implementation WBPreferencesController (Private)

- (void)_reloadSettings {
	[_quitOptionsMatrix selectCellAtRow:[[NSUserDefaults standardUserDefaults] integerForKey:WBQuitSavedUserChoiceType]
								 column:0];	
}

@end





@implementation WBPreferencesController


@synthesize quitOptionsMatrix = _quitOptionsMatrix;


#pragma mark -

- (id)init
{
    self = [super initWithWindowNibName:@"WBPreferences"];
    if (self) {
        [self _reloadSettings];
    }
    return self;
}





#pragma mark -

- (void)showWindow:(id)sender {
	[self _reloadSettings];
	
	[super showWindow:sender];
}




#pragma mark -

- (IBAction)saveOptionChange:(id)sender {
	NSInteger selectedTag;
		
	selectedTag = [sender tag];
	
	if(selectedTag < 1)
		[[NSUserDefaults standardUserDefaults] setBool:NO forKey:WBQuitRememberUserChoice];
	else
		[[NSUserDefaults standardUserDefaults] setBool:YES forKey:WBQuitRememberUserChoice];
	
	[[NSUserDefaults standardUserDefaults] setInteger:selectedTag forKey:WBQuitSavedUserChoiceType];
}



@end
