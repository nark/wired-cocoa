//
//  WBWatchersController.h
//  WireBot Manager
//
//  Created by Rafaël Warnault on 31/05/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBCollectionController.h"

@class WBWatcher;

@interface WBWatchersController : WBCollectionController {
	WBWatcher		*_currentWatcher;
}

@end
