//
//  WBCommand.m
//  WireBot Manager
//
//  Created by Rafaël Warnault on 31/05/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBCommand.h"
#import "WBOutput.h"

@implementation WBCommand


#pragma mark -

+ (NSArray *)privateCommandNames {
	return [NSArray arrayWithObjects:
			@"start", 
			@"stop", 
			@"sleep", 
			@"reload", 
			@"nick",
			@"status",
			@"help",
			nil];
}




#pragma mark -

- (id)init
{
    self = [super init];
    if (self) {
        _outputs	= [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc
{
	[_outputs release];
    [super dealloc];
}



#pragma mark -

- (id)copyWithZone:(NSZone *)zone {
	WBCommand		*command;
	
	command = [[[self class] allocWithZone:zone] init];
	
	[command setName:[self name]];
	[command setPermissions:[self permissions]];
	[command setActivated:[self isActivated]];
	[command setOutputs:[self outputs]];
	
	return command;
}






#pragma mark -

- (NSString *)name {
	return _name;
}

- (void)setName:(NSString *)name {
	if(_name)
		[_name release], _name = nil;
	
	_name = [name retain];
}


- (NSArray *)outputs {
	return _outputs;
}

- (void)setOutputs:(NSArray *)outputs {
	[_outputs setArray:outputs];
}

- (void)addOutput:(WBOutput *)output {
	[_outputs addObject:output];
}

- (BOOL)isPrivateCommand {
	return [[WBCommand privateCommandNames] containsObject:[self name]];
}




#pragma mark -

- (NSString *)outputsString {
	NSMutableString *string;
	
	string = [NSMutableString string];
	
	for(NSInteger i = 0; i < [[self outputs] count]; i++) {
		if([[[self outputs] objectAtIndex:i] contentValue]) {
			[string appendString:[[[self outputs] objectAtIndex:i] contentValue]];
			
			if(i != [[self outputs] count]-1)
				[string appendString:@", "];
		}
	}
	
	return string;
}




#pragma mark -

- (NSString *)XMLRepresentation {
	NSMutableString *string;
	
	string = [NSMutableString string];
	
	[string appendFormat:@"\t\t<command name=\"%@\" permissions=\"%@\" activated=\"%@\">\n", 
	 _name,
	 (_permissions)	? _permissions : @"any", 
	 (_activated)	? @"true" : @"false"];
	
	for(NSInteger i = 0; i < [[self outputs] count]; i++) {
		[string appendFormat:[[[self outputs] objectAtIndex:i] XMLRepresentation]];
		[string appendString:@"\n"];
	}
	
	[string appendString:@"\t\t</command>\n\n"];
	
	return string;
}

- (NSData *)XMLData {
	return [[self XMLRepresentation] dataUsingEncoding:NSUTF8StringEncoding];
}


@end
