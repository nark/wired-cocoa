//
//  WBCollectionController.h
//  WireBot Manager
//
//  Created by Rafaël Warnault on 30/05/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

@interface WBCollectionController : NSObject <NSXMLParserDelegate> {
	NSData					*_data;
	NSString				*_key;

	NSMutableArray			*_objects;
	
	NSXMLParser				*_parser;
}

#pragma mark -

- (id)initForKey:(NSString *)key;
- (id)initWithData:(NSData *)data forKey:(NSString *)key;

- (NSData *)data;
- (void)setData:(NSData *)data;


#pragma mark -

- (NSInteger)numberOfObjects;
- (id)objectAtIndex:(NSInteger)index;


#pragma mark -

- (void)reloadObjects;
- (void)reloadObjectsWithData:(NSData *)data;

- (void)addObject:(id)object;
- (void)removeObject:(id)object;

- (BOOL)containsObject:(id)object;

- (NSArray *)allObjects;
- (void)clearObjects;


#pragma mark -

- (BOOL)isEmpty;

- (NSString *)XMLRepresentation;
- (NSData *)XMLData;

@end
