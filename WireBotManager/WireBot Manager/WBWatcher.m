//
//  WBWatcher.m
//  WireBot Manager
//
//  Created by Rafaël Warnault on 31/05/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBWatcher.h"

@implementation WBWatcher

#pragma mark -

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}



#pragma mark -

- (NSString *)path {
	return _path;
}

- (void)setPath:(NSString *)path {
	if(_path)
		[_path release], _path = nil;
	
	_path = [path retain];
}



#pragma mark -

- (NSString *)XMLRepresentation {
	NSMutableString *string;
	
	string = [NSMutableString string];
	
	[string appendFormat:@"\t\t<watcher path=\"%@\" activated=\"%@\">\n", 
	 _path, 
	 (_activated)	? @"true" : @"false"];
	
	[string appendString:@"\t\t</watcher>\n\n"];
	
	return string;
}

- (NSData *)XMLData {
	return [[self XMLRepresentation] dataUsingEncoding:NSUTF8StringEncoding];
}

@end
