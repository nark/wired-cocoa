//
//  WBEditRuleController.h
//  WireBot Manager
//
//  Created by Rafaël Warnault on 31/05/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBSheetController.h"

@class WBRule, WBInput, WBOutput;

@interface WBEditRuleController : WBSheetController <NSTableViewDelegate, NSTableViewDataSource> {
	
	IBOutlet NSButton			*_activatedButton;
	IBOutlet NSTextField		*_permissionsTextField;
	
	IBOutlet NSTableView		*_inputsTableView;
	IBOutlet NSTableView		*_outputsTableView;
	
	IBOutlet NSButton			*_removeInputButton;
	IBOutlet NSButton			*_removeOutputButton;
	
	WBRule						*_editingRule;
	NSMutableArray				*_inputs;
	NSMutableArray				*_outputs;
}


@property (readwrite, retain)	WBRule				*editingRule;
@property (readwrite, retain)	NSMutableArray		*inputs;
@property (readwrite, retain)	NSMutableArray		*outputs;

- (IBAction)addInput:(id)sender;
- (IBAction)removeInput:(id)sender;

- (IBAction)addOutput:(id)sender;
- (IBAction)removeOutput:(id)sender;

@end
