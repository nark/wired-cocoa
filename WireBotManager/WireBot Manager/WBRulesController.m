//
//  WBRulesController.m
//  WireBot Manager
//
//  Created by Rafaël Warnault on 30/05/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBRulesController.h"
#import "WBRule.h"
#import "WBInput.h"
#import "WBOutput.h"

@implementation WBRulesController



#pragma mark -

- (void)dealloc
{
    [_currentRule release];
	[_currentInput release];
	[_currentOutput release];
    [super dealloc];
}



#pragma mark -

- (NSString *)XMLRepresentation {
	NSMutableString *string;
	
	string = [NSMutableString string];
	
	[string appendString:@"\t<rules>\n"];
	
	for(WBRule *rule in _objects) {
		[string appendString:[rule XMLRepresentation]];
	}
	
	[string appendString:@"\t</rules>\n\n"];
	
	return string;
}

- (NSData *)XMLData {
	NSMutableData *XMLData = [NSMutableData data];
		
	for(WBRule *rule in _objects) {
		[XMLData appendData:[rule XMLData]];
	}
	
	return XMLData;
}




#pragma mark -

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{	
	if([elementName isEqualToString:_key]) {
		if(!_currentRule) {
			_currentRule = [[WBRule alloc] init];
			
			if([attributeDict valueForKey:@"activated"])
				[_currentRule setActivated:[attributeDict boolForKey:@"activated"]];
			
			if([attributeDict valueForKey:@"permissions"])
				[_currentRule setPermissions:[attributeDict valueForKey:@"permissions"]];
			
			return;
		}
	}
	
	if([elementName isEqualToString:@"input"]) {
		if(_currentRule) {
			if(!_currentInput) {
				_currentInput = [[WBInput alloc] init];
				
				if([attributeDict valueForKey:@"message"])
					[_currentInput setMessage:[attributeDict valueForKey:@"message"]];
				
				if([attributeDict valueForKey:@"comparison"])
					[_currentInput setCaseSensitive:[WBInput comparisonMethodForString:[attributeDict valueForKey:@"comparison"]]];
				
				if([attributeDict valueForKey:@"sensitive"])
					[_currentInput setCaseSensitive:[attributeDict boolForKey:@"sensitive"]];
				
				
				return;
			}
		}
	}
	
	if([elementName isEqualToString:@"output"]) {
		if(_currentRule) {
			if(!_currentOutput) {
				_currentOutput = [[WBOutput alloc] init];
				
				if([attributeDict valueForKey:@"message"])
					[_currentOutput setMessage:[attributeDict valueForKey:@"message"]];
				
				if([attributeDict valueForKey:@"delay"])
					[_currentOutput setDelay:[attributeDict integerForKey:@"delay"]];
				
				if([attributeDict valueForKey:@"repeat"])
					[_currentOutput setRepeat:[attributeDict integerForKey:@"repeat"]];
				
				return;
			}
		}
	}
}


- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName  {
	if([elementName isEqualToString:_key]) {
		
		[_objects addObject:_currentRule];
		
		if(_currentRule)
			[_currentRule release], _currentRule = nil;
		
		return;
	}
	
	if([elementName isEqualToString:@"input"]) {
		if(_currentInput) {
			if(_currentRule)
				[_currentRule addInput:_currentInput];
			
			[_currentInput release], _currentInput = nil;
			
			return;
		}
	}

	if([elementName isEqualToString:@"output"]) {
		if(_currentOutput) {
			if(_currentRule)
				[_currentRule addOutput:_currentOutput];
			
			[_currentOutput release], _currentOutput = nil;
			
			return;
		}
	}
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string  {	
	if(!string || [string length] <= 0)
		return;
	
	if(_currentInput)
		[_currentInput setContentValue:string];
	
	if(_currentOutput)
		[_currentOutput setContentValue:string];
}




@end
