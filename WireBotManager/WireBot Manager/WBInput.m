//
//  WBInput.m
//  WireBot Manager
//
//  Created by Rafaël Warnault on 31/05/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBInput.h"

@implementation WBInput

+ (WBInputComparisonMethod)comparisonMethodForString:(NSString *)string {	
	if([string isEqualToString:@"equals"])
		return WBInputComparisonEquals;
		
	else if([string isEqualToString:@"contains"])
		return WBInputComparisonContains;
	
	else if([string isEqualToString:@"starts"])
		return WBInputComparisonStarts;
	
	else if([string isEqualToString:@"ends"])
		return WBInputComparisonEnds;
	
	return WBInputComparisonEquals;
}


+ (NSString *)stringForComparisonMethod:(WBInputComparisonMethod)method {	

	switch (method) {
		case WBInputComparisonEquals:	return @"equals";	break;
		case WBInputComparisonContains: return @"contains"; break;
		case WBInputComparisonStarts:	return @"starts";	break;
		case WBInputComparisonEnds:		return @"ends";		break;
		default:						return @"equals";	break;
	}
}



#pragma mark -

- (id)copyWithZone:(NSZone *)zone {
	WBInput		*input;
	
	input = [[[self class] allocWithZone:zone] init];
	
	[input setMessage:[self message]];
	[input setComparison:[self comparison]];
	[input setCaseSensitive:[self isCaseSensitive]];
	[input setContentValue:[self contentValue]];
	
	return input;
}



#pragma mark -

- (WBInputComparisonMethod)comparison {
	return _comparison;
}

- (void)setComparison:(WBInputComparisonMethod)comparison {
	_comparison = comparison;
}



- (BOOL)isCaseSensitive {
	return _caseSensitive;
}

- (void)setCaseSensitive:(BOOL)sensitive {
	_caseSensitive = sensitive;
}



#pragma mark -

- (NSString *)XMLRepresentation {
	return 	[NSString stringWithFormat:@"\t\t\t<input message=\"%@\" comparison=\"%@\" sensitive=\"%@\">%@</input>", 
			 _message ? _message : @"wired.chat.say",
			 [WBInput stringForComparisonMethod:_comparison],
			 _caseSensitive ? @"true" : @"false",
			 _value ? _value : @""];
}

- (NSData *)XMLData {
	return [[self XMLRepresentation] dataUsingEncoding:NSUTF8StringEncoding];
}


@end
