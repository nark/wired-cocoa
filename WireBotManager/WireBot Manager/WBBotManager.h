//
//  WBBotManager.h
//  WireBot Manager
//
//  Created by Rafaël Warnault on 05/04/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import <Foundation/Foundation.h>

#define	WBWirebotStatusDidChangeNotification		@"WBWirebotStatusDidChangeNotification"

@class WBError;

@interface WBBotManager : NSObject

@property (readwrite, retain)					NSTimer		*statusTimer;

@property (readwrite, getter = isRunning)       BOOL        running;
@property (readwrite, getter = isRunning)       NSInteger   pid;

- (BOOL)startWithError:(WBError **)error;
- (BOOL)stopWithError:(WBError **)error;

- (void)reload;

@end
