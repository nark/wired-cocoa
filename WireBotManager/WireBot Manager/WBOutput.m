//
//  WBOutput.m
//  WireBot Manager
//
//  Created by Rafaël Warnault on 31/05/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBOutput.h"


@implementation WBOutput


#pragma mark -

- (id)copyWithZone:(NSZone *)zone {
	WBOutput		*output;
	
	output = [[[self class] allocWithZone:zone] init];
	
	[output setMessage:[self message]];
	[output setDelay:[self delay]];
	[output setRepeat:[self repeat]];
	[output setContentValue:[self contentValue]];
	
	return output;
}





#pragma mark -

- (NSInteger)delay {
	return _delay;
}

- (void)setDelay:(NSInteger)delay {
	_delay = delay;
}


- (NSInteger)repeat {
	return _repeat;
}

- (void)setRepeat:(NSInteger)repeat {
	_repeat = repeat;
}



#pragma mark -

- (NSString *)XMLRepresentation {
	return 	[NSString stringWithFormat:@"\t\t\t<output message=\"%@\" delay=\"%d\" repeat=\"%d\">%@</output>", 
			 _message ? _message : @"wired.chat.say",
			 _delay,
			 _repeat,
			 _value];
}

- (NSData *)XMLData {
	return [[self XMLRepresentation] dataUsingEncoding:NSUTF8StringEncoding];
}

@end
