//
//  main.m
//  WireBot Manager
//
//  Created by Rafaël Warnault on 05/04/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
