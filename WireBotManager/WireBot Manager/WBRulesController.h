//
//  WBRulesController.h
//  WireBot Manager
//
//  Created by Rafaël Warnault on 30/05/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBCollectionController.h"

@class WBRule, WBInput, WBOutput;

@interface WBRulesController : WBCollectionController {
	WBRule		*_currentRule;
	WBInput		*_currentInput;
	WBOutput	*_currentOutput;
}

@end
