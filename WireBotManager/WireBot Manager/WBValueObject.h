//
//  WBResponseObject.h
//  WireBot Manager
//
//  Created by Rafaël Warnault on 31/05/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBXMLObject.h"


enum WBWiredMessageType {
	WBWiredMessageChatSayMessage		= 0,
	WBWiredMessageChatMeMessage			= 1,
	WBWiredMessagePrivateMessage		= 2,
	WBWiredMessageBroadcastMessage		= 3,
	WBWiredMessageUserJoindMessage		= 4,
	WBWiredMessageUserLeaveMessage		= 5
} typedef WBWiredMessageType;


@interface WBValueObject : WBXMLObject {
	NSString		*_value;
	NSString		*_message;
}

+ (NSString *)wiredMessageForMessageType:(WBWiredMessageType)type;
+ (WBWiredMessageType)messageTypeForWiredMessage:(NSString *)message;

- (NSString *)contentValue;
- (void)setContentValue:(NSString *)value;

- (NSString *)message;
- (void)setMessage:(NSString *)message;

@end
