//
//  WBDocument.m
//  WireBot Manager
//
//  Created by Rafaël Warnault on 03/06/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBDocument.h"
#import "WBAppDelegate.h"
#import "WBPreferencesController.h"
#import "WBBotManager.h"


@interface WBDocument (Private)

- (void)_reloadBot;


@end




@implementation WBDocument (Private)

- (void)_reloadBot {
	WBBotManager *manager;
	
	if([[NSUserDefaults standardUserDefaults] boolForKey:WBSaveAutoReloadWirebot]) {
		manager = [(WBAppDelegate *)[NSApp delegate] botManager];
		
		if(manager && [manager isRunning])
			[manager reload];
	}
}

@end




@implementation WBDocument

+ (BOOL)autosavesInPlace
{
    return YES;
}



#pragma mark -


- (void)saveDocument:(id)sender {
	[super saveDocument:sender];
	
	[self _reloadBot];
}

- (void)saveDocumentAs:(id)sender {
	[super saveDocumentAs:sender];
	
	[self _reloadBot];
}

- (void)saveDocumentTo:(id)sender {
	[super saveDocumentTo:sender];
	
	[self _reloadBot];
}


@end
