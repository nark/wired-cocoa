//
//  WBDocument.h
//  WireBot Manager
//
//  Created by Rafaël Warnault on 05/04/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "WBSheetController.h"
#import "WBDocument.h"

@class WBRulesController, WBCommandsController, WBWatchersController;
@class WBEditRuleController, WBEditCommandController;

@interface WBDictionaryDocument : WBDocument <NSTableViewDataSource, NSTableViewDelegate, WBSheetControllerDelegate> {
	NSToolbar				*_toolbar;
	NSTabView				*_tabView;
	
	NSTableView				*_rulesTableView;
	NSTableView				*_commandsTableView;
	NSTableView				*_watchersTableView;
	
	NSButton				*_addRuleButton;
	NSButton				*_removeRuleButton;
	NSPopUpButton			*_rulePopUpButton;
	
	NSButton				*_addCommandButton;
	NSButton				*_removeCommandButton;
	NSPopUpButton			*_commandPopUpButton;
	
	NSTextView				*_xmlTextView;
	
	WBEditRuleController	*_editRuleController;
	WBEditCommandController	*_editCommandController;
	
	WBRulesController		*_rulesController;
	WBCommandsController	*_commandsController;
	WBWatchersController	*_watchersController;
}

@property (assign) IBOutlet		NSToolbar				*toolbar;
@property (assign) IBOutlet		NSTabView				*tabView;

@property (assign) IBOutlet		NSTableView				*rulesTableView;
@property (assign) IBOutlet		NSTableView				*commandsTableView;
@property (assign) IBOutlet		NSTableView				*watchersTableView;

@property (assign) IBOutlet		NSButton				*addRuleButton;
@property (assign) IBOutlet		NSButton				*removeRuleButton;
@property (assign) IBOutlet		NSPopUpButton			*rulePopUpButton;

@property (assign) IBOutlet		NSButton				*addCommandButton;
@property (assign) IBOutlet		NSButton				*removeCommandButton;
@property (assign) IBOutlet		NSPopUpButton			*commandPopUpButton;

@property (assign) IBOutlet		NSTextView				*xmlTextView;

@property (assign) IBOutlet		WBEditRuleController	*editRuleController;
@property (assign) IBOutlet		WBEditCommandController	*editCommandController;

@property (readwrite, retain)	WBRulesController		*rulesController;
@property (readwrite, retain)	WBCommandsController	*commandsController;
@property (readwrite, retain)	WBWatchersController	*watchersController;

- (IBAction)switchView:(id)sender;

- (IBAction)addRule:(id)sender;
- (IBAction)editRule:(id)sender;
- (IBAction)removeRule:(id)sender;
- (IBAction)duplicateRule:(id)sender;

- (IBAction)addCommand:(id)sender;
- (IBAction)editCommand:(id)sender;
- (IBAction)removeCommand:(id)sender;
- (IBAction)duplicateCommand:(id)sender;

- (NSString *)XMLRepresentation;
- (NSData *)XMLData;

@end
