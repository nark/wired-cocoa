//
//  WBCollectionController.m
//  WireBot Manager
//
//  Created by Rafaël Warnault on 30/05/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBCollectionController.h"




@implementation WBCollectionController


#pragma mark -

- (id)initForKey:(NSString *)key {
    self = [super init];
    if (self) {
		_key		= [key retain];
		_objects	= [[NSMutableArray alloc] init];
    }
    return self;
}


- (id)initWithData:(NSData *)data forKey:(NSString *)key {
	self = [self initForKey:key];
    if (self) {
        _data		= [data retain];
		_parser		= [[NSXMLParser alloc] initWithData:_data];

		[_parser setDelegate:self];
		[_parser parse];
    }
    return self;
}


- (void)dealloc
{
	[_data release];
	[_key release];
	[_objects release];
	[_parser release];
    [super dealloc];
}




#pragma mark -

- (NSData *)data {
	return _data;
}


- (void)setData:(NSData *)data {
	if(_data)
		[_data release], _data = nil;
	
	if(_parser)
		[_parser release], _parser = nil;
	
	_data	= [data retain];
	_parser = [[NSXMLParser alloc] initWithData:_data];
	
	[_parser setDelegate:self];
	[_parser parse];
}





#pragma mark -

- (NSInteger)numberOfObjects {
	return [_objects count];
}


- (id)objectAtIndex:(NSInteger)index {
	return [_objects objectAtIndex:index];
}




#pragma mark -

- (void)reloadObjects {

}


- (void)reloadObjectsWithData:(NSData *)data {
	if(_data)
		[_data release], _data = nil;
	
	if(_parser)
		[_parser release], _parser = nil;
	
	_data		= [data retain];
	_parser		= [[NSXMLParser alloc] initWithData:_data];
	
	[_parser setDelegate:self];
	[self reloadObjects];
}


- (void)clearObjects {
	[_objects removeAllObjects];
}


- (void)addObject:(id)object {
	[_objects addObject:object];
}


- (void)removeObject:(id)object {
	[_objects removeObject:object];
}


- (BOOL)containsObject:(id)object {
	return [_objects containsObject:object];
}


- (NSArray *)allObjects {
	return _objects;
}




#pragma mark -

- (BOOL)isEmpty {
	return ([_objects count] > 0);
}

- (NSString *)XMLRepresentation {
	return nil;
}

- (NSData *)XMLData {
	return nil;
}


@end

