//
//  WBTriggerObject.h
//  WireBot Manager
//
//  Created by Rafaël Warnault on 30/05/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBXMLObject.h"

@interface WBTriggerObject : WBXMLObject {
	NSString		*_permissions;
	BOOL			_activated;
}

- (NSString *)permissions;
- (void)setPermissions:(NSString *)permissions;

- (BOOL)isActivated;
- (void)setActivated:(BOOL)activated;

@end
