//
//  WBCommandsController.m
//  WireBot Manager
//
//  Created by Rafaël Warnault on 31/05/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBCommandsController.h"
#import "WBCommand.h"
#import "WBOutput.h"


@implementation WBCommandsController


#pragma mark -

- (void)dealloc
{
    [_currentCommand release];
	[_currentOutput release];
    [super dealloc];
}



#pragma mark -

- (NSString *)XMLRepresentation {
	NSMutableString *string;
	
	string = [NSMutableString string];
	
	[string appendString:@"\t<commands>\n"];
	
	for(WBCommand *command in _objects) {
		[string appendString:[command XMLRepresentation]];
	}
	
	[string appendString:@"\t</commands>\n\n"];
	
	return string;
}

- (NSData *)XMLData {
	NSMutableData *XMLData = [NSMutableData data];
	
	for(WBCommand *command in _objects) {
		[XMLData appendData:[command XMLData]];
	}
	
	return XMLData;
}




#pragma mark -

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{	
	if([elementName isEqualToString:_key]) {
		if(!_currentCommand) {
			_currentCommand = [[WBCommand alloc] init];
			
			if([attributeDict valueForKey:@"name"])
				[_currentCommand setName:[attributeDict valueForKey:@"name"]];
			
			if([attributeDict valueForKey:@"activated"])
				[_currentCommand setActivated:[attributeDict boolForKey:@"activated"]];
			
			if([attributeDict valueForKey:@"permissions"])
				[_currentCommand setPermissions:[attributeDict valueForKey:@"permissions"]];
			
			return;
		}
	}
	
	if([elementName isEqualToString:@"output"]) {
		if(_currentCommand) {
			if(!_currentOutput) {
				_currentOutput = [[WBOutput alloc] init];
				
				if([attributeDict valueForKey:@"message"])
					[_currentOutput setMessage:[attributeDict valueForKey:@"message"]];
				
				if([attributeDict valueForKey:@"delay"])
					[_currentOutput setDelay:[attributeDict integerForKey:@"delay"]];
				
				if([attributeDict valueForKey:@"repeat"])
					[_currentOutput setRepeat:[attributeDict integerForKey:@"repeat"]];
				
				return;
			}
		}
	}
}


- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName  {
	if([elementName isEqualToString:_key]) {
		
		[_objects addObject:_currentCommand];
		
		if(_currentCommand)
			[_currentCommand release], _currentCommand = nil;
		
		return;
	}
	
	if([elementName isEqualToString:@"output"]) {
		if(_currentOutput) {
			if(_currentCommand)
				[_currentCommand addOutput:_currentOutput];
			
			[_currentOutput release], _currentOutput = nil;
			
			return;
		}
	}
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string  {	
	if(!string || [string length] <= 0)
		return;

	if(_currentOutput)
		[_currentOutput setContentValue:string];
}




@end
