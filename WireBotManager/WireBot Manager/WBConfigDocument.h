//
//  WBConfigDocument.h
//  WireBot Manager
//
//  Created by Rafaël Warnault on 03/06/12.
//  Copyright (c) 2012 OPALE. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "WBDocument.h"

@interface WBConfigDocument : WBDocument

@end
