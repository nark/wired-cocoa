//
//  WBEditRuleController.m
//  WireBot Manager
//
//  Created by Rafaël Warnault on 31/05/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBEditRuleController.h"
#import "WBRule.h"
#import "WBInput.h"
#import "WBOutput.h"
#import "WBValueObject.h"


@interface WBEditRuleController (Private)

- (void)_validate;

- (void)_clearSheet;
- (BOOL)_isSheetValid;

- (WBInput *)_selectedInput;
- (WBOutput *)_selectedOutput;

@end




@implementation WBEditRuleController (Private)


- (void)_validate {
	[_removeInputButton setEnabled:([self _selectedInput] != nil)];
	[_removeOutputButton setEnabled:([self _selectedOutput] != nil)];
}


- (void)_clearSheet {
	[_activatedButton setState:NSOffState];
	[_permissionsTextField setStringValue:@""];
	
	[_inputs removeAllObjects];
	[_outputs removeAllObjects];
	
	[_editingRule release], _editingRule = nil;
}


- (BOOL)_isSheetValid {
	return YES;
}


#pragma mark -

- (WBInput *)_selectedInput {
	WBInput		*selectedInput;
	NSInteger	selectedRow;
	
	selectedInput	= nil;
	selectedRow		= [_inputsTableView selectedRow];
	
	if(selectedRow != -1)
		selectedInput	= [_inputs objectAtIndex:selectedRow];
	
	return selectedInput;
}

- (WBOutput *)_selectedOutput {
	WBOutput	*selectedOutput;
	NSInteger	selectedRow;
	
	selectedOutput	= nil;
	selectedRow		= [_outputsTableView selectedRow];
	
	if(selectedRow != -1)
		selectedOutput	= [_outputs objectAtIndex:selectedRow];
	
	return selectedOutput;
}


@end






@implementation WBEditRuleController

@synthesize editingRule		= _editingRule;
@synthesize inputs			= _inputs;
@synthesize outputs			= _outputs;



- (id)init
{
    self = [super init];
    if (self) {
        _inputs = [[NSMutableArray alloc] init];
        _outputs = [[NSMutableArray alloc] init];
    }
    return self;
}


- (void)dealloc
{
    [_editingRule release];
	[_inputs release];
	[_outputs release];
	
    [super dealloc];
}






#pragma mark -

- (IBAction)addInput:(id)sender {
	WBInput *newInput;
	
	newInput = [[WBInput alloc] init];
	
	[newInput setContentValue:@":-)"];
	[newInput setMessage:@"wired.chat.say"];
	[newInput setComparison:WBInputComparisonEquals];
	[newInput setCaseSensitive:NO];
	
	[_inputs addObject:newInput];
	[_inputsTableView reloadData];
}


- (IBAction)removeInput:(id)sender {
	WBInput *selectedInput;
	
	selectedInput = [self _selectedInput];
	
	if(selectedInput)
		[_inputs removeObject:selectedInput];
	
	[_inputsTableView reloadData];
}



- (IBAction)addOutput:(id)sender {
	WBOutput *newOutput;
	
	newOutput = [[WBOutput alloc] init];
	
	[newOutput setContentValue:@":-)"];
	[newOutput setMessage:@"wired.chat.say"];
	[newOutput setRepeat:0];
	[newOutput setDelay:0];
	
	[_outputs addObject:newOutput];
	[_outputsTableView reloadData];
}


- (IBAction)removeOutput:(id)sender {
	WBOutput *selectedOutput;
	
	selectedOutput = [self _selectedOutput];
	
	if(selectedOutput)
		[_outputs removeObject:selectedOutput];
	
	[_outputsTableView reloadData];
}




#pragma mark -

- (void)openSheet:(id)sender {
	
	if(_editingRule) {
		[_activatedButton setState:[_editingRule isActivated]];
		[_permissionsTextField setStringValue:[_editingRule permissions]];
		
		[_inputs addObjectsFromArray:[_editingRule inputs]];
		[_outputs addObjectsFromArray:[_editingRule outputs]];
	}
	
	[_inputsTableView reloadData];
	[_outputsTableView reloadData];
	
	[self _validate];
	
	[super openSheet:sender];
}

- (void)cancelSheet:(id)sender {
	[super cancelSheet:sender];
	[self _clearSheet];
}


- (void)messageDelegateAndCloseSheet:(id)sender {
	
	if(!_editingRule)
		_editingRule = [[WBRule alloc] init];
	
	[_editingRule setActivated:[_activatedButton state]];
	[_editingRule setPermissions:[_permissionsTextField stringValue]];
	[_editingRule setInputs:_inputs];
	[_editingRule setOutputs:_outputs];
	
	[super messageDelegateAndCloseSheet:_editingRule];
	[self _clearSheet];
}



#pragma mark -


- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
	if(tableView == _inputsTableView) {
		return [_inputs count];
	
	} else if(tableView == _outputsTableView) {
		return [_outputs count];
	}
	return 0;
}

- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
	if(tableView == _inputsTableView) {
		
		if(!_inputs || [_inputs count] == 0)
			return nil;
		
		if([[tableColumn identifier] isEqualToString:@"value"])
			return [[_inputs objectAtIndex:row] contentValue];
		
		else if([[tableColumn identifier] isEqualToString:@"message"])
			return [NSNumber numberWithInteger:[WBInput messageTypeForWiredMessage:[[_inputs objectAtIndex:row] message]]];
		
		else if([[tableColumn identifier] isEqualToString:@"comparison"])
			return [NSNumber numberWithInteger:[[_inputs objectAtIndex:row] comparison]];
		
		else if([[tableColumn identifier] isEqualToString:@"sensitive"])
			return [NSNumber numberWithBool:[[_inputs objectAtIndex:row] isCaseSensitive]];
		
	} else if(tableView == _outputsTableView) {
		
		if(!_outputs || [_outputs count] == 0)
			return nil;
		
		if([[tableColumn identifier] isEqualToString:@"value"])
			return [[_outputs objectAtIndex:row] contentValue];
		
		else if([[tableColumn identifier] isEqualToString:@"message"])
			return [NSNumber numberWithInteger:[WBOutput messageTypeForWiredMessage:[[_outputs objectAtIndex:row] message]]];
		
		else if([[tableColumn identifier] isEqualToString:@"delay"])
			return [NSNumber numberWithInteger:[[_outputs objectAtIndex:row] delay]];
		
		else if([[tableColumn identifier] isEqualToString:@"repeat"])
			return [NSNumber numberWithBool:[[_outputs objectAtIndex:row] repeat]];
	}
	return nil;
}


- (void)tableView:(NSTableView *)tableView setObjectValue:(id)object forTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
	if(tableView == _inputsTableView) {
		if([[tableColumn identifier] isEqualToString:@"value"])
			[[_inputs objectAtIndex:row] setContentValue:object];
			
		else if([[tableColumn identifier] isEqualToString:@"message"])
			[[_inputs objectAtIndex:row] setMessage:[WBInput wiredMessageForMessageType:[object integerValue]]];
			
		else if([[tableColumn identifier] isEqualToString:@"comparison"])
			[[_inputs objectAtIndex:row] setComparison:[object integerValue]];
		
		else if([[tableColumn identifier] isEqualToString:@"sensitive"])
			[[_inputs objectAtIndex:row] setCaseSensitive:[object boolValue]];
			
	} else if(tableView == _outputsTableView) {
		if([[tableColumn identifier] isEqualToString:@"value"])
			[[_outputs objectAtIndex:row] setContentValue:object];
			
		else if([[tableColumn identifier] isEqualToString:@"message"])
			[[_outputs objectAtIndex:row] setMessage:[WBOutput wiredMessageForMessageType:[object integerValue]]];
			
		else if([[tableColumn identifier] isEqualToString:@"delay"])
			[[_outputs objectAtIndex:row] setDelay:[object integerValue]];
			
		else if([[tableColumn identifier] isEqualToString:@"repeat"])
			[[_outputs objectAtIndex:row] setRepeat:[object integerValue]];
	}
	
	[self _validate];
}

- (void)tableViewSelectionDidChange:(NSNotification *)notification {
	[self _validate];
}



@end
