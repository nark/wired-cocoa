//
//  WBTriggerObject.m
//  WireBot Manager
//
//  Created by Rafaël Warnault on 30/05/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBTriggerObject.h"

@implementation WBTriggerObject


#pragma mark -

- (void)dealloc
{
    [_permissions release];
    [super dealloc];
}




#pragma mark -

- (NSString *)permissions {
	return _permissions;
}

- (void)setPermissions:(NSString *)permissions {
	if(_permissions)
		[_permissions release], _permissions = nil;
	
	_permissions = [permissions retain];
}



- (BOOL)isActivated {
	return _activated;
}

- (void)setActivated:(BOOL)activated {
	_activated = activated;
}

@end
