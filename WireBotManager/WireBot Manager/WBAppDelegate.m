//
//  WBAppDelegate.m
//  WireBot Manager
//
//  Created by Rafaël Warnault on 05/04/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBAppDelegate.h"
#import "WBPreferencesController.h"
#import "WBBotManager.h"
#import "WBDocument.h"
#import "WBDictionaryDocument.h"
#import "WBConfigDocument.h"
#import "WBError.h"


@interface WBAppDelegate (Private)

- (void)_updateStatusMenu;

- (WBDocument *)_documentForURL:(NSURL *)url;

- (void)_quitAll:(BOOL)all;
- (void)_showQuitAlert;

@end



@implementation WBAppDelegate


#pragma mark -

@synthesize statusItem				= _statusItem;
@synthesize quitAccessoryView		= _quitAccessoryView;
@synthesize documentsController		= _documentsController;
@synthesize statusMenu				= _statusMenu;
@synthesize botManager				= _botManager;
@synthesize preferencesController	= _preferencesController;







#pragma mark -

+ (void)initialize {
	// init NSUserDefaults defaults settings
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	NSMutableDictionary *defaults = [[[NSMutableDictionary alloc] init] autorelease];
	
	[defaults setObject:[NSNumber numberWithBool:YES] forKey:WBSaveAutoReloadWirebot];
	[defaults setObject:[NSNumber numberWithBool:NO] forKey:WBQuitRememberUserChoice];
    [defaults setObject:[NSNumber numberWithInteger:WBQuitUserChoiceAskMe] forKey:WBQuitSavedUserChoiceType];
	
	[userDefaults registerDefaults:defaults];
}





#pragma mark -

- (id)init {
    self = [super init];
    if (self) {
        _statusItem				= [[[NSStatusBar systemStatusBar] statusItemWithLength:30.0] retain];
        _botManager				= [[WBBotManager alloc] init];
		_documentsController	= [[NSDocumentController alloc] init];
    }
    return self;
}


- (void)dealloc {
    [_statusItem release];
    [_statusMenu release];
    [_botManager release];
	[_documentsController release];
	
    [super dealloc];
}





#pragma mark -

- (void)applicationDidFinishLaunching:(NSNotification *)notification {
    [self.statusItem setHighlightMode:YES];
    [self.statusItem setMenu:self.statusMenu];
	
	[[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(wirebotStatusDidChange:) 
												 name:WBWirebotStatusDidChangeNotification];
    
    [self _updateStatusMenu];
}

- (BOOL)applicationShouldOpenUntitledFile:(NSApplication *)sender {
    return NO;
}





#pragma mark -

- (void)wirebotStatusDidChange:(NSNotification *)notification {
	 [self _updateStatusMenu];
}





#pragma mark -

- (IBAction)start:(id)sender {
    WBError		*error;
    [_botManager startWithError:&error];
    
    [self _updateStatusMenu];
}


- (IBAction)stop:(id)sender {
    WBError		*error;
    [_botManager stopWithError:&error];
    
    [self _updateStatusMenu];
}


- (IBAction)reload:(id)sender {
	[_botManager reload];
}





#pragma mark -

- (IBAction)newDictionaryDocument:(id)sender {
	WBDictionaryDocument	*newDocument;
	
	newDocument = [[WBDictionaryDocument alloc] init];
	
	[newDocument makeWindowControllers];
	[newDocument showWindows];
	
	[_documentsController addDocument:newDocument];
	
	[newDocument release];
}


- (IBAction)newConfigDocument:(id)sender {
	WBConfigDocument		*newDocument;
	
	newDocument = [[WBConfigDocument alloc] init];
	
	[newDocument makeWindowControllers];
	[newDocument showWindows];
	
	[_documentsController addDocument:newDocument];
	
	[newDocument release];
}


- (IBAction)openDictionaryOrConfigDocuments:(id)sender {
	NSOpenPanel				*openPanel;
	WBDocument				*newDocument;
	NSError					*error;
	int						result;
	
	openPanel	= [NSOpenPanel openPanel];	
	[openPanel setAllowedFileTypes:[NSArray arrayWithObjects:@"xml", @"conf", nil]];
	
	result		= [openPanel runModalForDirectory:[NSHomeDirectory() stringByAppendingPathComponent:@".wirebot"] 
										 file:nil 
										types:nil];
	
	if (result == NSOKButton) {
		
		if([[[[openPanel URL] path] pathExtension] isEqualToString:@"xml"]) {
			newDocument = [[WBDictionaryDocument alloc] initWithContentsOfURL:[openPanel URL] 
																   ofType:@"xml" 
																	error:&error];
			
		} else if([[[[openPanel URL] path] pathExtension] isEqualToString:@"conf"]) {
			newDocument = [[WBConfigDocument alloc] initWithContentsOfURL:[openPanel URL] 
																   ofType:@"conf" 
																	error:&error];
		}
		
		[newDocument makeWindowControllers];
		[newDocument showWindows];
		
		[_documentsController addDocument:newDocument];
		
		[newDocument release];
	}
}




#pragma mark -

- (IBAction)editConfig:(id)sender {
	WBConfigDocument		*newDocument;
	NSError					*error;
	NSURL					*url;

	url = [NSURL fileURLWithPath:[[NSHomeDirectory() stringByAppendingPathComponent:@".wirebot"] 
								  stringByAppendingPathComponent:@"wirebot.conf"]];
	
	
	newDocument = (WBConfigDocument *)[self _documentForURL:url];
	
	if(!newDocument)	{
		newDocument = [[[WBConfigDocument alloc] initWithContentsOfURL:url 
																ofType:@"conf" 
																 error:&error] autorelease];
		
		[newDocument makeWindowControllers];
	}
	
	[_documentsController addDocument:newDocument];
	[newDocument showWindows];
}


- (IBAction)editDictionary:(id)sender {
	WBDictionaryDocument	*newDocument;
	NSError					*error;
	NSURL					*url;
	
	url = [NSURL fileURLWithPath:[[NSHomeDirectory() stringByAppendingPathComponent:@".wirebot"] 
								  stringByAppendingPathComponent:@"wirebot.xml"]];
	
	newDocument = (WBDictionaryDocument *)[self _documentForURL:url];
	
	if(!newDocument) {
		newDocument = [[[WBDictionaryDocument alloc] initWithContentsOfURL:url
																	ofType:@"xml" 
																	 error:&error] autorelease];
		
		[newDocument makeWindowControllers];
	}

	[_documentsController addDocument:newDocument];
	[newDocument showWindows];
}


- (IBAction)quit:(id)sender {
	
	if([_botManager isRunning]) {
		if(![[NSUserDefaults standardUserDefaults] boolForKey:WBQuitRememberUserChoice]) {
			
			[self _showQuitAlert];
			
		} else {
			WBQuitUserChoiceType choice = [[NSUserDefaults standardUserDefaults] integerForKey:WBQuitSavedUserChoiceType];
			
			switch (choice) {
				case WBQuitUserChoiceAskMe:		[self _showQuitAlert];		break;
				case WBQuitUserChoiceQuit:		[self _quitAll:NO];			break;
				case WBQuitUserChoiceQuitAll:	[self _quitAll:YES];		break;
				default:						[self _showQuitAlert];		break;
			}
		}
	} else {
		[NSApp terminate:self];
	}
}




#pragma mark -

- (void)menuWillOpen:(NSMenu *)menu {
    
    NSMenuItem      *item;
    
    [menu removeAllItems];
    
    if(![_botManager isRunning]) {
        item = [menu addItemWithTitle:@"Start Wirebot…" action:@selector(start:) keyEquivalent:@""];
        [item setTarget:self];
		
    } else {
        item = [menu addItemWithTitle:@"Stop Wirebot…" action:@selector(stop:) keyEquivalent:@""];
        [item setTarget:self];
		
		item = [menu addItemWithTitle:@"Reload" action:@selector(reload:) keyEquivalent:@""];
		[item setTarget:self];
    }
    
    [menu addItem:[NSMenuItem separatorItem]];
	
    item = [menu addItemWithTitle:@"Edit Wirebot Config…" action:@selector(editConfig:) keyEquivalent:@""];
    [item setTarget:self];
    
    item = [menu addItemWithTitle:@"Edit Wirebot Dictionary…" action:@selector(editDictionary:) keyEquivalent:@""];
    [item setTarget:self];
	
	[menu addItem:[NSMenuItem separatorItem]];
	
	item = [menu addItemWithTitle:@"Quit WiredBot Manager" action:@selector(quit:) keyEquivalent:@""];
    [item setTarget:self];
}



#pragma mark -

- (void)_updateStatusMenu {
    if([_botManager isRunning]) {
        [self.statusItem setImage:[NSImage imageNamed:@"WiredServerMenu"]];
	} else {
        [self.statusItem setImage:[NSImage imageNamed:@"WiredServerMenuOff"]];
    }
}


- (WBDocument *)_documentForURL:(NSURL *)url {
	for(WBDocument *document in [_documentsController documents]) {
		if([[[document fileURL] path] isEqualToString:[url path]])
			return document;
	}
	return nil;
}





#pragma mark -

- (void)_quitAll:(BOOL)all {
	WBError			*error;
	
	if(all && [_botManager isRunning])
		[_botManager stopWithError:&error];
	
	[NSApp terminate:self];
}



- (void)_showQuitAlert {
	NSAlert			*alert;
	NSInteger		result;
	
	[NSApp activateIgnoringOtherApps:YES];
	
	alert = [NSAlert alertWithMessageText:@"Quit Warning"
							defaultButton:@"Quit"
						  alternateButton:@"Cancel"
							  otherButton:@"Stop the Bot and Quit"
				informativeTextWithFormat:@"Do you also want to stop the Wirebot process ? If you choose the \"Quit\" option, Wirebot will continue to run as a daemon."];
	
	[alert setAccessoryView:_quitAccessoryView];
	
	result = [alert runModal];
	
	if(result == NSAlertDefaultReturn) {
		if([[NSUserDefaults standardUserDefaults] boolForKey:WBQuitRememberUserChoice])
			[[NSUserDefaults standardUserDefaults] setInteger:WBQuitUserChoiceQuit forKey:WBQuitSavedUserChoiceType];
			
		[self _quitAll:NO];
		
	} else if(result == NSAlertOtherReturn) {
		if([[NSUserDefaults standardUserDefaults] boolForKey:WBQuitRememberUserChoice])
			[[NSUserDefaults standardUserDefaults] setInteger:WBQuitUserChoiceQuitAll forKey:WBQuitSavedUserChoiceType];
		
		[self _quitAll:YES];
	}
}


@end
