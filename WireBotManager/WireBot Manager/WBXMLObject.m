//
//  WBXMLObject.m
//  WireBot Manager
//
//  Created by Rafaël Warnault on 30/05/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBXMLObject.h"

@implementation WBXMLObject




- (void)dealloc
{
    [_parent release];
	[_children release];
    [super dealloc];
}




#pragma mark -

- (WBXMLObject *)parent {
	return _parent;
}

- (void)setParent:(WBXMLObject *)parent {
	if(_parent)
		[_parent release], _parent = nil;
	
	_parent = [parent retain];
}



- (NSMutableArray *)children {
	return _children;
}

- (void)setChildren:(NSArray *)children {
	if(_children)
		[_children release], _children = nil;
	
	_children = [[NSMutableArray arrayWithArray:children] retain];
}



#pragma mark -

- (void)addChild:(WBXMLObject *)child {
	[_children addObject:child];
}

- (void)addChildren:(NSArray *)children {
	[_children addObjectsFromArray:children];
}


- (void)removeChild:(WBXMLObject *)child {
	[_children removeObject:child];
}

- (void)removeChildren:(NSArray *)children {
	[_children removeObjectsInArray:children];
}




#pragma mark -

- (NSString *)XMLRepresentation {
	return nil;
}

- (NSData *)XMLData {
	return nil;
}


@end
