//
//  WBAppDelegate.h
//  WireBot Manager
//
//  Created by Rafaël Warnault on 05/04/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import <Foundation/Foundation.h>


@class WBBotManager, WBPreferencesController;

@interface WBAppDelegate : NSObject <NSApplicationDelegate, NSMenuDelegate>

@property (assign)              IBOutlet NSMenu						*statusMenu;
@property (assign)              IBOutlet NSView						*quitAccessoryView;
@property (assign)				IBOutlet WBPreferencesController	*preferencesController;

@property (readwrite, retain)	NSDocumentController				*documentsController;

@property (readwrite, retain)   NSStatusItem						*statusItem;
@property (readwrite, retain)   WBBotManager						*botManager;

- (IBAction)start:(id)sender;
- (IBAction)stop:(id)sender;
- (IBAction)reload:(id)sender;

- (IBAction)newDictionaryDocument:(id)sender;
- (IBAction)newConfigDocument:(id)sender;
- (IBAction)openDictionaryOrConfigDocuments:(id)sender;

- (IBAction)editConfig:(id)sender;
- (IBAction)editDictionary:(id)sender;

- (IBAction)quit:(id)sender;

@end
