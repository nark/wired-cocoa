//
//  WBBotManager.m
//  WireBot Manager
//
//  Created by Rafaël Warnault on 05/04/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBBotManager.h"
#import "WBError.h"

#import <signal.h>

@interface WBBotManager (Private)
- (BOOL)_reloadPidFile;
@end



@implementation WBBotManager


#pragma mark -

@synthesize statusTimer = _statusTimer;
@synthesize running		= _running;
@synthesize pid			= _pid;





#pragma mark -

- (id)init {
    self = [super init];
    if (self) {
        [self _reloadPidFile];
		
		_statusTimer = [[NSTimer scheduledTimerWithTimeInterval:1.0
														 target:self
													   selector:@selector(statusTimer:)
													   userInfo:NULL
														repeats:YES] retain];
		
		[_statusTimer fire];
    }
    return self;
}

- (void)dealloc
{
    [_statusTimer release];
    [super dealloc];
}






#pragma mark -

- (BOOL)startWithError:(WBError **)error {
    NSTask			*task;
	
	task = [[[NSTask alloc] init] autorelease];
	[task setLaunchPath:WB_BINARY_PATH];
	[task setArguments:[NSArray arrayWithObjects:@"-D", nil]];
	[task setStandardOutput:[NSPipe pipe]];
	[task setStandardError:[task standardOutput]];
	[task launch];
	
    self.running = YES;
    
	return YES;
}

- (BOOL)stopWithError:(WBError **)error {
    
    [self _reloadPidFile];
    
    kill((int)_pid, SIGINT);
    
    self.running = NO;
    return YES;
}



- (void)reload {
	[self _reloadPidFile];
    
    kill((int)_pid, SIGHUP);
}





#pragma mark -

- (void)statusTimer:(NSTimer *)timer  {
	BOOL		notify = NO;
	
	if([self _reloadPidFile])
		notify = YES;
	
	if([self isRunning]) {
		notify = YES;
	}
	
	if(notify)
		[[NSNotificationCenter defaultCenter] postNotificationName:WBWirebotStatusDidChangeNotification 
															object:self];
}





#pragma mark -

- (BOOL)_reloadPidFile {
	NSString		*string, *command;
	BOOL			running = NO;
	
	string = [NSString stringWithContentsOfFile:[WB_USER_PATH stringByAppendingPathComponent:@"wirebot.pid"]
									   encoding:NSUTF8StringEncoding
										  error:NULL];
	
	if(string) {
		command = [[NSWorkspace sharedWorkspace] commandForProcessIdentifier:[string unsignedIntValue]];
		
		if([command isEqualToString:@"wirebot"]) {
			_pid = [string unsignedIntegerValue];
			
			running = YES;
		} else {
            [[NSFileManager defaultManager] removeItemAtPath:[WB_USER_PATH stringByAppendingPathComponent:@"wirebot.pid"]
                                                       error:nil];
		}
	}
    	
	if(running != _running) {
		_running = running;
		
		return YES;
	}
	
	return NO;
}



@end
