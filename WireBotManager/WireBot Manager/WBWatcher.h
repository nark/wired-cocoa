//
//  WBWatcher.h
//  WireBot Manager
//
//  Created by Rafaël Warnault on 31/05/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import "WBTriggerObject.h"

@class WBOutput;

@interface WBWatcher : WBTriggerObject {
	NSMutableArray		*_outputs;
	NSString			*_path;
}

- (NSString *)path;
- (void)setPath:(NSString *)path;

@end
