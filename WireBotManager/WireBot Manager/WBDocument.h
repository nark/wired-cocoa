//
//  WBDocument.h
//  WireBot Manager
//
//  Created by Rafaël Warnault on 03/06/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface WBDocument : NSDocument

@end
