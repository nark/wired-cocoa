//
//  WBXMLObject.h
//  WireBot Manager
//
//  Created by Rafaël Warnault on 30/05/12.
//  Copyright (c) 2012 Read-Write.fr. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WBXMLObject : NSObject {
	WBXMLObject			*_parent;
	NSMutableArray		*_children;
}

- (WBXMLObject *)parent;
- (void)setParent:(WBXMLObject *)parent;

- (NSMutableArray *)children;
- (void)setChildren:(NSArray *)children;

- (void)addChild:(WBXMLObject *)child;
- (void)addChildren:(NSArray *)children;

- (void)removeChild:(WBXMLObject *)child;
- (void)removeChildren:(NSArray *)children;

- (NSString *)XMLRepresentation;
- (NSData *)XMLData;

@end
