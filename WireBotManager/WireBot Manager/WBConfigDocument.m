//
//  WBConfigDocument.m
//  WireBot Manager
//
//  Created by Rafaël Warnault on 03/06/12.
//  Copyright (c) 2012 OPALE. All rights reserved.
//

#import "WBConfigDocument.h"





@implementation WBConfigDocument


#pragma mark -

- (id)init
{
    self = [super init];
    if (self) {

    }
    return self;
}




#pragma mark -

- (NSString *)windowNibName
{
    return @"WBConfigDocument";
}


- (void)windowControllerDidLoadNib:(NSWindowController *)aController
{
    [super windowControllerDidLoadNib:aController];

}


- (NSData *)dataOfType:(NSString *)typeName error:(NSError **)outError
{

    return nil;
}


- (BOOL)readFromData:(NSData *)data ofType:(NSString *)typeName error:(NSError **)outError
{
    return YES;
}


@end
